from flask import Flask
from flask_restful import Resource, Api, reqparse, fields, marshal_with
from flask_sqlalchemy import SQLAlchemy
import datetime
from services.hashgen import hash_new_password
import hmac

#resource_fields = {
#    'alias': fields.String,
#    'email': fields.String,
#    'phone': fields.Integer,
#    'passwd': fields.String,
#    'conf_passwd': fields.String,
#    'first_name': fields.String,
#    'last_name': fields.String,
#}

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://root:repulsin@127.0.0.1/pyauth'
db = SQLAlchemy(app)
api = Api(app)

parser = reqparse.RequestParser()
parser.add_argument('alias')
parser.add_argument('email')
parser.add_argument('phone')
parser.add_argument('passw')
parser.add_argument('conf_passw')
parser.add_argument('first_name')
parser.add_argument('last_name')
parser.add_argument('middle_name')

class Users(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    chat_id = db.Column(db.Integer, unique=True, index=True)
    alias = db.Column(db.String(64), unique=True, index=True)
    email = db.Column(db.String(64), unique=True, index=True)
    phone = db.Column(db.Integer, unique=True, index=True)
    passw = db.Column(db.String(128))
    first_name = db.Column(db.String(64), index=True)
    last_name = db.Column(db.String(64))
    middle_name = db.Column(db.String(64))
    active = db.Column(db.Boolean(), index=True)
    role = db.Column(db.Integer, index=True)
    salt = db.Column(db.String(32))
    two_factor_email = db.Column(db.Boolean(), index=True)
    two_factor_telegram = db.Column(db.Boolean(), index=True)
    telegram_key_token = db.Column(db.String(64))
    Session_key = db.Column(db.String(64))
    created_at = db.Column(db.DateTime())
    updated_at = db.Column(db.DateTime())

    #def __repr__(self):
    #    return f"Name : {self.first_name}, Alias: {self.alias}"

#def hash_new_password(password: str) -> Tuple[bytes, bytes]:
#    salt = os.urandom(32)
#    pw_hash = hashlib.pbkdf2_hmac('sha256', password.encode('utf-8'), salt, 100000)
#    return salt, pw_hash

class HomePage(Resource):
    def get(self):
        print(db.session.query(Users).count())
        users = Users.query.all()
        return {'app_name':'pyauth'}

class SignUp(Resource):
    #@marshal_with(resource_fields)
    def post(self):

        args = parser.parse_args()

        if db.session.query(Users).filter(Users.email == args.email).count() > 0:
            return {'Error': 'Email exist'}
        if db.session.query(Users).filter(Users.phone == args.phone).count() > 0:
            return {'Error': 'Phone used'}
        if db.session.query(Users).filter(Users.alias == args.alias).count() > 0:
            return {'Error': 'Alias is taken'}

        if args.passw == args.conf_passw:
            hash_salt, hash_pass = hash_new_password(args.passw)

            print(f"{hash_pass}")
            new_user = Users(
                alias = args.alias,
                email = args.email,
                phone = args.phone,
                first_name = args.first_name,
                last_name = args.last_name,
                middle_name = args.middle_name,
                passw = args.passw, #hash_pass.encode('utf-8').strip(),
                salt = '123', #hash_salt.encode('utf-8').strip(),
                created_at = datetime.datetime.now(),
                updated_at = datetime.datetime.now())
            try:
                db.session.add(new_user)
                db.session.commit()
            except:
                #raise
                return {'Error' : 'DB exception'}
            print(new_user.id)
            return {'New User ID' : new_user.id}
        else:
            return {'Error': 'Pass and Conf Pass are not match'}

class GetOTP (Resource):
    def post(self):
        args = parser.parse_args()
        if len(args.email) > 0 and len(args.passw) > 0:
            user = db.session.query(Users).first()
            return {'ID' : user.id}
            for user_obj in user:
                print(f"{user_obj.first_name}{user_obj.last_name}")
            return {'email' : len(args.email), 'passw' : len(args.passw)}
        return {'email' : len(args.email), 'passw' : len(args.passw)}


api.add_resource(HomePage,'/','/home')
api.add_resource(SignUp, '/signup')
api.add_resource(GetOTP, '/signin')

#https://bitbucket.org/archyfindev/pyauth/
#https://github.com/wspcapital/Auth2FA
if __name__ == '__main__':
    app.run(debug=True)
