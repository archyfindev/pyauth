import os
from typing import Tuple
import hashlib

def hash_new_password(password: str) -> Tuple[bytes, bytes]:
    salt = os.urandom(32)
    pw_hash = hashlib.pbkdf2_hmac('sha256', password.encode('utf-8'), salt, 100000)
    return salt, pw_hash